require 'test_helper'
require "custom_test_case"

class UsersControllerTest < CustomTestCase

  def setup
    @controller = Api::V1::UsersController.new
    load_seeds
    @user = autenticated_user
  end

  test "Show Autenticated user" do

    get :show, {:format => :json, id: @user.id, "access-token" => @token, "client" => @client_id, "uid" => @user.email}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal @user.email, json["uid"]

  end

  test "Update Name User" do

    put :update, {:format => :json, id: @user.id, "access-token" => @token, "client" => @client_id, "uid" => @user.email, "user" => {"name" => "Jhon Doe"}}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal @user.id, json["id"]

  end


  test "Update Photo User" do

    test_image = Rails.root.join('public', 'images', 'users', 'futbol-pelota-humanos-cabeza-silueta-cara.png').to_s
    file = Rack::Test::UploadedFile.new(test_image, "image/jpeg")

    put :update, {:format => :json, id: @user.id, "access-token" => @token, "client" => @client_id, "uid" => @user.email, "user" => {"photo" => file}}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal @user.id, json["id"]

    assert_match /player-football-icon/, @user.photo.url

  end


  test "Update Cut Day and SMS by Cut" do

    put :update, {:format => :json, id: @user.id, "access-token" => @token, "client" => @client_id, "uid" => @user.email, "user" => {"cut_day" => 10, "sms_by_cut" => 1000}}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal @user.id, json["id"]

    @user.reload

    assert_equal @user.cut_day, 10
    assert_equal @user.sms_by_cut, 1000

  end


end
