require 'test_helper'
require "custom_test_case"

class MessagesControllerTest < CustomTestCase

  def setup
    @controller = Api::V1::MessagesController.new
    load_seeds
    @user = autenticated_user
  end

  test "Get Messages of User" do

    get :index, {:format => :json, "access-token" => @token, "client" => @client_id, "uid" => @user.email}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal 1, json["data"].length

  end

end
