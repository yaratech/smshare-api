require 'test_helper'
require "custom_test_case"

class AuthPasswordControllerTest < CustomTestCase

  def setup
    
    request.env['devise.mapping'] = Devise.mappings[:user_sender]

    @controller = Api::V1::DeviseTokenAuth::PasswordsController.new

    load_seeds

    @user = UserSender.first

  end

  test "Request Password reset" do
    get :create, {email: @user.email}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal "Su nueva contraseña ha sido enviada al correo #{@user.email}.", json["data"]["message"]

  end

  test "Fail Request Password" do
    post :create, {email: ""}
    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "La Direccion de Correo Electronico no Existe.", json["errors"][0]

  end

  test "Change Password" do

    @user = autenticated_user

    put :update, {email: @user.email, password: "87654321", "access-token" => @token, "client" => @client_id, "uid" => @user.email}
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal @user.email, json["data"]["user"]["uid"]    

  end  

  test "Fail Change Password" do

    @user = autenticated_user

    put :update, {email: @user.email, password: "876543", "access-token" => @token, "client" => @client_id, "uid" => @user.email}
    json = JSON.parse(response.body)

    assert_response :unprocessable_entity

    assert_equal false, json["success"]

    assert_equal "es demasiado corto (mínimo 8 caracteres)", json["errors"]["password"][0]


    put :update, {email: "", password: "", "access-token" => @token, "client" => @client_id, "uid" => @user.email}
    json = JSON.parse(response.body)

    assert_response :unprocessable_entity

    assert_equal false, json["success"]

    assert_equal "no puede estar en blanco", json["errors"]["password"][0]


  end  

end
