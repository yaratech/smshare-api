require 'test_helper'
require "custom_test_case"

class QueueControllerTest < CustomTestCase

  def setup
    @controller = Api::V1::QueueController.new
    load_seeds
    @user = autenticated_user
  end

  test "Get Message in Queue" do

    assert_difference 'Resque.size(:test_sms)', -1 do
      get :index, {:format => :json, "access-token" => @token, "client" => @client_id, "uid" => @user.email}
    end

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal 1, json["data"].length

  end

  test "Queue Empty" do
    Resque.redis.del "queue:test_sms"

    get :index, {:format => :json, "access-token" => @token, "client" => @client_id, "uid" => @user.email}

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal false, json["success"]

    assert_equal "Sin Mensajes en cola", json["info"]

  end

  test "Message sent" do

    assert_difference 'Resque.size(:test_sms)', -1 do
      get :index, {:format => :json, "access-token" => @token, "client" => @client_id, "uid" => @user.email}
    end

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal 1, json["data"].length

    id = json["data"]["message"]["id"]

    put :update, {
      :format => :json, 
      id: id,
      "access-token" => @token, 
      "client" => @client_id, 
      "uid" => @user.email, 
      "message" => {
        "sent" => true,
        "sent_at" => "2017-07-28T14:05:49.795-04:00",
        "in_transit" => false
      }
    }

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal id, json["id"]

    msg = Message.find(id)

    assert_equal true, msg.sent
    assert_equal false, msg.in_transit
    assert_equal 1, msg.sender.sender.sent
    assert_equal -1, msg.sender.sender.balance

  end

end
