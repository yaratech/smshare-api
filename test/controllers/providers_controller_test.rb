require 'test_helper'
require "custom_test_case"

class ProvidersControllerTest < CustomTestCase

  def setup
    @controller = Api::V1::ProvidersController.new

    request.env['devise.mapping'] = Devise.mappings[:user_sender]

    load_seeds

  end

  test "get list users without autentication" do
    get :index
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal 4, json["data"]["user"].length

  end

  test "Create user with Facebook Login" do

    post :create, {
        :format => :json, 
        email: "facebookABC455@gmail.com",
        userid: "234234",
        name: "pedro perez",
        access_token: "abcdefghy",
        push_token: "lSHupJVYNOCLxzuPA5SdhP1mfLgUCVe2mG"
    }

    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal "facebookabc455@gmail.com", json["data"]["user"]["uid"]

    assert_equal true, (response.header.key?("Access-Token") || response.header.key?("access-token"))
    assert_equal true, (response.header.key?("Client") || response.header.key?("client"))
    assert_equal true, (response.header.key?("uid") || response.header.key?("Uid"))

  end

  test "Fail Creation of user with Facebook Login" do

    # without name
    post :create, {
        :format => :json, 
        email: "facebook455@gmail.com",
        userid: "234234",
        access_token: "abcdefghy",
        push_token: "lSHupJVYNOCLxzuPA5SdhP1mfLgUCVe2mG"
    }

    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Debe proporcionar el nombre.", json["errors"].first

    # with only access_token
    post :create, {
        :format => :json, 
        access_token: "abcdefghy"
    }

    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Debe proporcionar el UserID.", json["errors"].first

    # with access_token and userid
    post :create, {
        :format => :json, 
        access_token: "abcdefghy",
        userid: "234234",
    }

    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Debe proporcionar el nombre.", json["errors"][0]

    # without any value
    post :create, {
        :format => :json
    }

    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Debe proporcionar el access token.", json["errors"][0]

  end


end
