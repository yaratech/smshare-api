require 'test_helper'

class AuthSessionControllerTest < ActionController::TestCase

  def setup

    Rails.application.load_seed

    request.env['devise.mapping'] = Devise.mappings[:user_sender]

    @controller = Api::V1::DeviseTokenAuth::SessionsController.new

  end

  test "Autenticate A Existent User" do

    post :create, { email: "juan@dominio.com", password: "12345678", push_token: "abcdefgh" }
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal "Juan Rodriguez", json["data"]["name"]

    assert_equal true, (response.header.key?("Access-Token") || response.header.key?("access-token"))
    assert_equal true, (response.header.key?("Client") || response.header.key?("client"))
    assert_equal true, (response.header.key?("uid") || response.header.key?("Uid"))    

    post :create, { email: "JUAN@dominio.com", password: "12345678", push_token: "abcdefgh" }
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal true, json["success"]

    assert_equal "Juan Rodriguez", json["data"]["name"]

    assert_equal true, (response.header.key?("Access-Token") || response.header.key?("access-token"))
    assert_equal true, (response.header.key?("Client") || response.header.key?("client"))
    assert_equal true, (response.header.key?("uid") || response.header.key?("Uid"))    

  end

  test "Fail autentication" do

    # testing user don't exist
    post :create, { email: "luelher@gmail.com", password: "12345678", push_token: "abcdefgh", name: "luis"}
    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Identidad o contraseña no válida.", json["errors"][0]

    # testing without info
    post :create, { email: "", password: "", push_token: "", name: "luis"}
    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Identidad o contraseña no válida.", json["errors"][0]

    # testing with no params
    post :create, {}
    json = JSON.parse(response.body)

    assert_response :unauthorized

    assert_equal false, json["success"]

    assert_equal "Identidad o contraseña no válida.", json["errors"][0]


  end  
end
