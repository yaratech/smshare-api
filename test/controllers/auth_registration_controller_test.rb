require 'test_helper'

class AuthRegistrationControllerTest < ActionController::TestCase

  def setup

    Rails.application.load_seed

    request.env['devise.mapping'] = Devise.mappings[:user_sender]

    @controller = Api::V1::DeviseTokenAuth::RegistrationsController.new

  end

  test "User Registration" do

    post :create, { email: "luelher@gmail.com", password: "12345678", push_token: "abcdefgh", name: "Luis Hernández"}
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal "success", json["status"]

    assert_equal "luelher@gmail.com", json["data"]["uid"]
    assert_match /player-football-icon/, json["data"]["photo_url"]

    assert_equal true, (response.header.key?("Access-Token") || response.header.key?("access-token"))
    assert_equal true, (response.header.key?("Client") || response.header.key?("client"))
    assert_equal true, (response.header.key?("uid") || response.header.key?("Uid"))

    post :create, { email: "Heller@gmail.com", password: "12345678", push_token: "abcdefgh", name: "Luis Hernández"}
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal "success", json["status"]

    assert_equal "heller@gmail.com", json["data"]["uid"]


    assert_equal true, (response.header.key?("Access-Token") || response.header.key?("access-token"))
    assert_equal true, (response.header.key?("Client") || response.header.key?("client"))
    assert_equal true, (response.header.key?("uid") || response.header.key?("Uid"))

    post :create, { email: "  Jesus@gmail.com  ", password: "12345678", push_token: "abcdefgh", name: "Luis Hernández"}
    json = JSON.parse(response.body)

    assert_response :success

    assert_equal "success", json["status"]

    assert_equal "jesus@gmail.com", json["data"]["uid"]

    assert_equal true, (response.header.key?("Access-Token") || response.header.key?("access-token"))
    assert_equal true, (response.header.key?("Client") || response.header.key?("client"))
    assert_equal true, (response.header.key?("uid") || response.header.key?("Uid"))

  end

  test "Fail registration" do

    # testing user don't exist
    post :create, { email: "luelher@gmail.com", password: "123456", push_token: "", name: "Luis Hernández"}
    json = JSON.parse(response.body)

    assert_response :unprocessable_entity

    assert_equal "error", json["status"]

    assert_equal "Contraseña es demasiado corto (mínimo 8 caracteres)", json["errors"]["full_messages"][0]

    # testing without info
    post :create, { email: "", password: "", push_token: ""}
    json = JSON.parse(response.body)

    assert_response :unprocessable_entity

    assert_equal "error", json["status"]

    assert_equal "Contraseña no puede estar en blanco", json["errors"]["full_messages"][0]
    assert_equal "Correo Electrónico no puede estar en blanco", json["errors"]["full_messages"][1]
    assert_equal "Correo Electrónico no es un correo electrónico", json["errors"]["full_messages"][2]
    assert_equal "Nombre no puede estar en blanco", json["errors"]["full_messages"][3]

    # testing with no params
    post :create, {}
    json = JSON.parse(response.body)

    assert_response :unprocessable_entity

    assert_equal "error", json["status"]

    assert_equal "Los datos introducidos en la solicitud de acceso no son válidos.", json["errors"][0]

  end  
end
