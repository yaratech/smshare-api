require 'test_helper'
require 'mocha/test_unit'
require "rake"

class AlertsTaskTest < ActiveSupport::TestCase

  def setup
    Rails.application.load_seed
  end

  test "send one notifications" do

    FCM.any_instance.stubs(:send).returns(true).at_least_once
    Rake::Task["alerts:welcome"].invoke

  end

  test "create test sms in queue" do

    assert_difference 'Resque.size(:test_sms)', 10 do
      Rake::Task["test:create_messages"].invoke("10","04245558074")
    end

  end


end