namespace :alerts do

  desc "Tasks For Schedule"

  task :welcome => :environment do
    puts "Notifications welcome_crackapp"
    require 'fcm'
    fcm = FCM.new(FCM_KEY)
    @title = "Dominio"       

    @TokenDevice = TokenDevice.joins(:user).includes(:user).where("users.push_on=true")

    @TokenDevice.each_with_index do |device, i|   
      registration_ids = []  
      @body = "Gracias #{device.user.name} por usar Dominio, Visita http://dominio.com para saber más"
      options = {:notification=>{:body=>@body, :title=>@title}}                    
      registration_ids[i] = device.push_token
      fcm.send(registration_ids, options)
    end

  end

end

namespace :test do
  desc "Schedule Test Messages"

  task :create_messages, [:amount, :phone_number] => :environment do |t, args|

    if args[:amount] && args[:phone_number]
      applicant_id = UserApplicant.first.id
      (1..args[:amount].to_i).to_a.each do |sms|
        obj_msj = Message.new
        obj_msj.applicant_id = applicant_id
        obj_msj.message = "Mensaje de Prueba #{sms}"
        obj_msj.in_queue = true
        obj_msj.phone_number = args[:phone_number]
        obj_msj.in_queue_at = Time.now
        obj_msj.save
        QueueSmsJob.perform_later(obj_msj.id)
      end
    else
      puts "Debe indicar la cantidad de mensajes y el numero al cual se enviarán"
    end

  end
end
