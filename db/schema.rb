# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170729025035) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "applicants", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "sent"
    t.integer  "queue"
    t.integer  "for_send"
    t.float    "balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "applicants", ["user_id"], name: "index_applicants_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "applicant_id"
    t.string   "phone_number"
    t.string   "message"
    t.boolean  "in_queue",      default: false
    t.datetime "in_queue_at"
    t.boolean  "sent",          default: false
    t.datetime "sent_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "in_transit",    default: false
    t.datetime "in_transit_at"
  end

  add_index "messages", ["applicant_id"], name: "index_messages_on_applicant_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "senders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "sent"
    t.integer  "queue"
    t.float    "balance"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "cut_day",    default: 0
    t.integer  "sms_by_cut", default: 0
  end

  add_index "senders", ["user_id"], name: "index_senders_on_user_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "token_devices", force: :cascade do |t|
    t.string   "push_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "token_devices", ["push_token", "user_id"], name: "index_token_devices_on_push_token_and_user_id", unique: true, using: :btree
  add_index "token_devices", ["user_id"], name: "index_token_devices_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "name",                   default: "",      null: false
    t.string   "type",                                     null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "tokens"
    t.string   "status"
    t.datetime "reset_password_date"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "user_id_provider"
    t.boolean  "profile"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "push_on",                default: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "token_devices", "users"
end
