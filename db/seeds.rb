# migrate user
# puts "Users"
seed_file = Rails.root.join('db', 'seeds', 'users.yml')
config = YAML::load_file(seed_file)
User.create!(config)

# migrate Match
first_user = UserSender.first
last_user = UserSender.last

@client_id = SecureRandom.urlsafe_base64(nil, false)
@token     = SecureRandom.urlsafe_base64(nil, false)
@token_crypt = BCrypt::Password.create(@token)

first_user.tokens[@client_id] = {
  token: BCrypt::Password.create(@token),
  expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
}        
first_user.token_devices << TokenDevice.new(push_token: "12345678")
first_user.save


admin = UserAdmin.new
admin.name = "Administrador"
admin.email = "admin@dominio.com"
admin.password = '12345678'
admin.type = "UserAdmin"
admin.status = 'A'

admin.save

seed_file = Rails.root.join('db', 'seeds', 'messages.yml')
config = YAML::load_file(seed_file)
config.each do |sms|
  sms['applicant_id'] = UserApplicant.first.id
  obj_msj = Message.create!(sms)
  obj_msj.in_queue = true
  obj_msj.in_queue_at = Time.now
  obj_msj.save
  QueueSmsJob.perform_later(obj_msj.id)
end
puts "Seeds executed"