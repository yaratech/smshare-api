class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :sender_id, index: true, foreign_key: true
      t.integer :applicant_id, index: true, foreign_key: true
      t.string :phone_number, default: nil
      t.string :message, default: nil
      t.boolean :in_queue, default: false
      t.datetime :in_queue_at, default: nil
      t.boolean :sent, default: false
      t.datetime :sent_at, default: nil

      t.timestamps null: false
    end
  end
end
