class AddPushOnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :push_on, :boolean, default: true
  end
end
