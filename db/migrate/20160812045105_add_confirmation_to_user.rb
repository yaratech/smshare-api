class AddConfirmationToUser < ActiveRecord::Migration
  def change
    add_column :users, :confirmation, :boolean
    add_column :users, :married, :boolean
  end
  def self.down  
    remove_column :users, :confirmation
    remove_column :users, :married
  end
end
