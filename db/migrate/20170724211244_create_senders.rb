class CreateSenders < ActiveRecord::Migration
  def change
    create_table :senders do |t|
      t.references :user, index: true
      t.integer :sent
      t.integer :queue
      t.float :balance

      t.timestamps null: false
    end
  end
end
