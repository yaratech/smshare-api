class AddProfileUser < ActiveRecord::Migration
  def change
    add_column :users, :profile, :boolean
  end
  def self.down  
    remove_column :users, :profile   
  end
end
