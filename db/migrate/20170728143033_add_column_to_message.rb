class AddColumnToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :in_transit, :boolean, default: false
    add_column :messages, :in_transit_at, :datetime, default: nil
  end
end
