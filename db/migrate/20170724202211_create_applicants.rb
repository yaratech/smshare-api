class CreateApplicants < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.references :user, index: true
      t.integer :sent
      t.integer :queue
      t.integer :for_send
      t.float :balance

      t.timestamps null: false
    end
  end
end
