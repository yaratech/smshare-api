class DelFieldUser < ActiveRecord::Migration
  def change
    remove_column :users, :confirmation
    remove_column :users, :married
    remove_attachment :users, :photo
    remove_column :users, :confirmation_token
    remove_column :users, :confirmed_at
    remove_column :users, :confirmation_sent_at
    remove_column :users, :unconfirmed_email
    remove_column :users, :facebook_key
    remove_column :users, :twitter_key
    remove_column :users, :gender
    remove_column :users, :baptized
    remove_column :users, :first_communion
  end
  def self.down  
    add_column :users, :confirmation, :boolean
    add_column :users, :married, :boolean
    add_attachment :users, :photo
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime
    add_column :users, :unconfirmed_email, :string
    add_column :users, :facebook_key, :string
    add_column :users, :twitter_key, :string
    add_column :users, :gender, :string
    add_column :users, :baptized, :boolean
    add_column :users, :first_communion, :boolean
  end
end
