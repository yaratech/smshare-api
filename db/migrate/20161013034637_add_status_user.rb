class AddStatusUser < ActiveRecord::Migration
  def self.up
    add_column :users, :status, :string
    add_column :users, :reset_password_date, :datetime
  end
  def self.down
     remove_column :users, :status
     remove_column :users, :reset_password_date
  end
end
