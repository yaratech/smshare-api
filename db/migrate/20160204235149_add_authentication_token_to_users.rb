class AddAuthenticationTokenToUsers < ActiveRecord::Migration
  def change
    # add_column :users, :authentication_token, :string
    # add_index :users, :authentication_token

      ## Required
    add_column :users, :provider, :string , :null => false, :default => "email"
    add_column :users, :uid, :string, :null => false, :default => ""
      ## Tokens
    add_column :users, :tokens, :string 

  end
  def self.down  
    remove_column :users, :provider
    remove_column :users, :uid
    remove_column :users, :tokens
  end
end
