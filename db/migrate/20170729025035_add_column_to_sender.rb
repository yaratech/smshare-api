class AddColumnToSender < ActiveRecord::Migration
  def change
    add_column :senders, :cut_day, :integer, default: 0
    add_column :senders, :sms_by_cut, :integer, default: 0
  end
end
