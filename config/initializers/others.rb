require Rails.root.join('lib', 'extras', 'string.rb').to_s
require Rails.root.join('lib', 'extras', 'devise_fallback_handler.rb').to_s
require Rails.root.join('lib', 'extras', 'union_scope.rb').to_s
require Rails.root.join('app', 'jobs', 'queue_sms_job.rb').to_s
