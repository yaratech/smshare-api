Rails.application.routes.draw do

  
  devise_for :admin, :class_name => 'UserAdmin', :skip => [:registrations], controllers: { sessions: 'sessions' }

  mount_devise_token_auth_for 'UserSender', at: 'api/v1/auth', skip: [:omniauth_callbacks], controllers: {
                                                                                                registrations:      'api/v1/devise_token_auth/registrations',
                                                                                                passwords:          'api/v1/devise_token_auth/passwords',
                                                                                                sessions:          'api/v1/devise_token_auth/sessions',
                                                                                              }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # You can have the root of your site routed with "root"
  root to: "home#index"

  namespace :api do  

    namespace :v1 do                
        get "users/me" => 'users#me', :defaults => { :format => 'json' }
        resources :users, only: [:show, :update], :defaults => { :format => 'json' } 
        post "auth/providers" => 'providers#create', :defaults => { :format => 'json' }   
        get "users" => 'providers#index', :defaults => { :format => 'json' }     
        resources :queue, only: [:index, :update], :defaults => { :format => 'json' }
        resources :messages, only: [:index], :defaults => { :format => 'json' } 
    end
  end
end
