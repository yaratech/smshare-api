# config valid only for current version of Capistrano
lock "3.9.0"

set :application, "SmShare"
set :repo_url, "git@bitbucket.org:yaratech/smshare-api.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/rastreala/smshare/api"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rvm_type, :user
set :rvm_ruby_version, '2.2.0@smshare'

set :keep_releases, 2

set :slackistrano, {
 channel: '#notificaciones',
 webhook: 'https://hooks.slack.com/services/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'
}

append :linked_files, 'config/database.yml', 'config/secrets.yml'

namespace :deploy do
  after :finishing, 'reload'
  after :rollback, 'reload'
end

desc 'reload application'
task :reload do
  on roles(:app), in: :sequence, wait: 5 do
    execute :touch, release_path.join('tmp/restart.txt')
  end
end  
