class UserSender < User
  include UserSenderAdmin

  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  after_create :create_sender
  
  scope :just_senders, ->{where("type = 'UserSender'")}

  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: ActionController::Base.helpers.image_url("player-football-icon.png")
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/

  #validates_confirmation_of :password

  validates :name, presence: true

  #belongs_to :country
  has_many :token_devices, dependent: :destroy, :foreign_key => "user_id"
  has_one :sender, :foreign_key => "user_id"

  #before_create :default_values
  before_create :set_status
  before_create :generate_reset_password_token # generating devise reset token


  def as_json(options={})
    super.as_json(options).merge({        
        photo_url: photo_file_name.nil? ? ActionController::Base.helpers.image_url("player-football-icon.png") : photo.url,
        sender: sender
    })
  end

  def unique_email_user
    if provider == 'email' and self.class.where(provider: 'email', email: email).count > 0
      errors.add(:email, :already_in_use, default: "ya está en uso")
    end
  end

  def set_status
    self.status = "A"   
  end


  def cut_day
    sender.cut_day if sender
  end

  def cut_day=(val)
    if sender
      sender.update_attributes(cut_day: val)
    else
      sender = Sender.create(cut_day: val)
    end
  end

  def sms_by_cut
    sender.sms_by_cut if sender
  end

  def sms_by_cut=(val)
    if sender
      sender.update_attributes(sms_by_cut: val)
    else
      sender = Sender.create(sms_by_cut: val)
    end
  end


  private
    def generate_reset_password_token
      raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
      @raw_confirmation_token   = raw
      self.reset_password_token   = enc
      self.reset_password_sent_at = Time.now.utc
    end

    def create_sender
      self.sender = Sender.new(sent: 0, queue: 0, balance: 0)
      self.sender.save
    end

end
