class UserApplicant < User
  include UserApplicantAdmin

  #acts_as_token_authenticatable

  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  # include DeviseTokenAuth::Concerns::Applicant
  
  scope :just_applicants, ->{where("type = 'UserApplicant'")}

  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: ActionController::Base.helpers.image_url("player-football-icon.png")
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/

  #validates_confirmation_of :password

  validates :name, presence: true

  #belongs_to :country
  has_many :token_devices, dependent: :destroy

  #before_create :default_values
  before_create :set_status
  before_create :generate_reset_password_token # generating devise reset token


  def as_json(options={})
    super.as_json(options).merge({        
        photo_url: photo_file_name.nil? ? ActionController::Base.helpers.image_url("player-football-icon.png") : photo.url
    })
  end

  def unique_email_user
    if provider == 'email' and self.class.where(provider: 'email', email: email).count > 0
      errors.add(:email, :already_in_use, default: "ya está en uso")
    end
  end

  def set_status
    self.status = "A"   
  end

  private
    def generate_reset_password_token
      raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
      @raw_confirmation_token   = raw
      self.reset_password_token   = enc
      self.reset_password_sent_at = Time.now.utc
    end

end
