module UserAdminAdmin
  extend ActiveSupport::Concern

  included do
    rails_admin do
      label "Administrador" 
      label_plural "Administradores"
      list do
        field :id
        field :name
        field :email
      end
      edit do
        field :name
        field :email
        field :password
        field :password_confirmation
      end
    end
  end
end