module UserSenderAdmin
  extend ActiveSupport::Concern

  included do
    rails_admin do
      label "Transmisor" 
      label_plural "Transmisores"
      list do
        field :id
        field :name
        field :email
      end
      edit do
        field :name
        field :email
        field :password
        field :password_confirmation
        field :type        
        field :status
        field :provider
        field :tokens
        field :profile
        field :push_on
      end
    end
  end
end