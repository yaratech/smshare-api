class Message < ActiveRecord::Base
  belongs_to :sender, class_name: 'UserSender'
  belongs_to :applicant, class_name: 'UserApplicant'

  def add_sender_sent
    sender.sender.sent += 1
    sender.sender.balance -= 1
    sender.sender.save
  end

  def add_sender_queue
    sender.queue += 1
  end

end
