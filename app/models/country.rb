class Country < ActiveRecord::Base
  has_many :users, class_name: "Users::User", dependent: :destroy
end
