class Api::V1::QueueController < Api::V1::BaseController
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :set_user_by_token
  before_action :authenticate_user_sender!

  respond_to :json

  def index

    # get sms from queue
    job = Resque.reserve("#{Rails.env}_sms".to_sym)
    if job
      id = job.args.first["arguments"].first
      if id
        message = Message.find(id)
        message.sender_id = current_user_sender.id
        message.in_queue = false
        message.in_transit = true
        message.in_transit_at = Time.now
        message.save
        return render json: {
          success: true,
          data: {
            message: message
            }
        }
      end
    else
      return render json: {
        success: false,
        info: "Sin Mensajes en cola"
      } 
    end

  end

  # PATCH/PUT /api/{plural_resource_name}/1
  def update
    unless get_resource.sent
      if get_resource.update(resource_params)
        get_resource.add_sender_sent
        render json: {
            success: true,
            id: get_resource.id
          }, status: :accepted
      else
        render json: {
            success: false,
            errors: get_resource.errors
          }, status: :unprocessable_entity
      end
    else
      render json: {
          success: false,
          errors: "Ya fue enviado"
        }, status: :unprocessable_entity
    end
  end


private

  def queue_params
    params.require(:message).permit(
      :sent, 
      :sent_at,
      :in_transit)
  end

  def resource_klass
    @resource_klass = Message
  end  

end