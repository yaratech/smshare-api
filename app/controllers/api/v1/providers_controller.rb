class Api::V1::ProvidersController  < Api::V1::BaseController
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :set_user_by_token, only: [:create]
  #before_action :authenticate_users_user!, only: [:create]

  #respond_to :json

    # GET /Users
  # GET /Users.json
  def index
    @user = UserSender.where(type: "UserSender")
                       .page(page_params[:page])
                       .per(page_params[:page_size])  

    return render json: {
      success: true,
      data: {
        user: @user         
      }
    }
  end

  # POST /Providers
  # POST /providers.json
  def create   
    @email = resource_params[:email]
    @email = @email.to_s.strip.downcase
    

    unless resource_params[:access_token]
      return render json: {
        success: false,
        errors: ['Debe proporcionar el access token.']
      }, status: 401
    end   
    unless resource_params[:userid]
      return render json: {
        success: false,
        errors: ['Debe proporcionar el UserID.']
      }, status: 401
    end
    unless  @email
      return render json: {
        success: false,
        errors: ['Debe proporcionar una dirección de correo electrónico.']
      }, status: 401
    end



    @user_facebook = UserSender.where({
              email:  @email
            }).first
    
    #byebug

    if @user_facebook
       

        @client_id = SecureRandom.urlsafe_base64(nil, false)
        @token     = SecureRandom.urlsafe_base64(nil, false)
        @user_facebook.tokens[@client_id] = {
          token: BCrypt::Password.create(@token),
          expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
        }
        @token_crypt = BCrypt::Password.create(@token)
        @expiry = (Time.now + DeviseTokenAuth.token_lifespan).to_i  


       if @user_facebook.save

         @push_token = TokenDevice.where("push_token = ? and user_id = ?", resource_params[:push_token], @user_facebook.id)
          if @push_token.count == 0
           @tokendevice = TokenDevice.new() 
           @tokendevice.push_token = params[:push_token]
           @tokendevice.user_id = @user_facebook.id
           @tokendevice.save
          end 

          sign_in(:user, @user_facebook, store: false, bypass: false)
          
          self.headers['Access-Token'] = @token
          self.headers['Client'] = @client_id
          self.headers['uid'] = @user_facebook.uid

         return render json: {
            success: true,
            data: {
              user: @user_facebook,
              message: "Usuario Logeado con Exito. "           
            }
          }

        else
          return render json: {
            success: false,
            errors: @user_facebook.errors,
          }, status: 422
       end
      
    else
        unless resource_params[:name]
          return render json: {
            success: false,
            errors: ['Debe proporcionar el nombre.']
          }, status: 401
        end
       #@client = UserSender.new(resource_params)
       @user = UserSender.new()
       @user.email =  @email
       @user.uid =  @email
       @user.name = resource_params[:name]
       @user.user_id_provider = resource_params[:userid]
       @user.oauth_token = resource_params[:access_token]   
       password = SecureRandom.urlsafe_base64(nil, false)
       password_encryptada = BCrypt::Password.create(password)
       @user.password = password_encryptada
       @user.type = 'UserSender'
       @user.provider = 'facebook'
       @user.profile   = false
       #byebug


        @client_id = SecureRandom.urlsafe_base64(nil, false)
        @token     = SecureRandom.urlsafe_base64(nil, false)
        @user.tokens[@client_id] = {
          token: BCrypt::Password.create(@token),
          expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
        }
        @token_crypt = BCrypt::Password.create(@token)
        @expiry = (Time.now + DeviseTokenAuth.token_lifespan).to_i   
        
       if @user.save
          #token device
          @push_token = TokenDevice.where("push_token = ? and user_id = ?", resource_params[:push_token], @user.id)
          if @push_token.count == 0
           @tokendevice = TokenDevice.new() 
           @tokendevice.push_token = params[:push_token]
           @tokendevice.user_id = @user.id
           @tokendevice.save
          end           
          #byebug
          sign_in(:user, @user, store: false, bypass: false)
          
          self.headers['Access-Token'] = @token
          self.headers['Client'] = @client_id
          self.headers['uid'] = @user.uid

          ActionCorreo.user_create(@user).deliver_now
          return render json: {
            success: true,
            data: {
              user: @user,
              message: "Usuario creado con Realizado con Exito..."
            }
          }
       else
          return render json: {
            success: false,
            errors: @user.errors,
          }, status: 422
       end

    end

  end


  private

    def page_params
      params.permit(:page, :page_size)
    end    
      
    def resource_params
      params.permit(:access_token, :userid, :email, :name, :push_token)
    end
end

