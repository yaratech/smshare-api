module Api::V1::DeviseTokenAuth
  class RegistrationsController < DeviseTokenAuth::RegistrationsController

    def create
      the_params = sign_up_params      
      #country = Country.find_by_code(the_params[:country_id])
      #if country 
      #  the_params[:country_id] = country.id
      #else
      #  the_params[:country_id] = nil
      #end

      the_params[:type] = 'UserSender'

      @resource            = resource_class.new(the_params)

      
      @resource.provider   = "email"

      @resource.profile   = false

      # honor devise configuration for case_insensitive_keys
      if resource_class.case_insensitive_keys.include?(:email)
        @resource.email = the_params[:email].try :downcase
      else
        @resource.email = the_params[:email]
      end

      # give redirect value from params priority
      @redirect_url = params[:confirm_success_url]

      # fall back to default value if provided
      @redirect_url ||= DeviseTokenAuth.default_confirm_success_url

      # success redirect url is required
      if resource_class.devise_modules.include?(:confirmable) && !@redirect_url
        return render_create_error_missing_confirm_success_url
      end

      # if whitelist is set, validate redirect_url against whitelist
      if DeviseTokenAuth.redirect_whitelist
        unless DeviseTokenAuth.redirect_whitelist.include?(@redirect_url)
          return render_create_error_redirect_url_not_allowed
        end
      end

      begin
        # override email confirmation, must be sent manually from ctrl
        resource_class.skip_callback("create", :after, :send_on_create_confirmation_instructions)
        if @resource.save
          yield @resource if block_given?

          unless @resource.confirmed?
            # user will require email authentication
            @resource.send_confirmation_instructions({
              client_config: params[:config_name],
              redirect_url: @redirect_url
            })

          else
            # email auth has been bypassed, authenticate user
            @client_id = SecureRandom.urlsafe_base64(nil, false)
            @token     = SecureRandom.urlsafe_base64(nil, false)

            @resource.tokens[@client_id] = {
              token: BCrypt::Password.create(@token),
              expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
            }

            @resource.save!

            update_auth_header

            #token device
             @push_token = TokenDevice.where("push_token = ? and user_id = ?", params[:push_token], @resource.id)
             if @push_token.count == 0
               @tokendevice = TokenDevice.new() 
               @tokendevice.push_token = params[:push_token]
               @tokendevice.user_id = @resource.id
               @tokendevice.save
             end  

          end
          render_create_success          
          ActionCorreo.user_create(@resource).deliver_now
        else
          clean_up_passwords @resource
          render_create_error
        end
      rescue ActiveRecord::RecordNotUnique
        clean_up_passwords @resource
        render_create_error_email_already_exists
      end
    end
  end
end
