module Api::V1::DeviseTokenAuth
  class PasswordsController < DeviseTokenAuth::PasswordsController   
    include DeviseTokenAuth::Concerns::SetUserByToken

    before_action :set_user_by_token, only: [:update]
    before_action :authenticate_user_sender!, only: [:update]


   
    def create
      #byebug
      unless resource_params[:email]
        return render json: {
          success: false,
          errors: ['Debe proporcionar una dirección de correo electrónico.']
        }, status: 401
      end

      @user = resource_class.where({
        email: resource_params[:email].downcase,
        provider: 'email'
      }).first


      if @user


        password = SecureRandom.urlsafe_base64(nil, false)
        password_encryptada = BCrypt::Password.create(password)       
        params_password = {"encrypted_password"=>password_encryptada,"status"=>'R', "reset_password_date"=>Time.now}
        
        #byebug
        if @user.update_attributes(params_password)
          ActionCorreo.request_password(@user, password).deliver_now
          return render json: {
            success: true,
            data: {
              user: @user.email,
              message: "Su nueva contraseña ha sido enviada al correo #{ @user.email }."
            }
          }
        else
          return render json: {
            success: false,
            errors: @user.errors
          }, status: 422
        end
      else
        return render json: {
            success: false,
            errors: ['La Direccion de Correo Electronico no Existe.']
          }, status: 401       
      end


    end # fin create

    def update
      #byebug
      unless resource_params[:password]
        return render json: {
          success: false,
          errors: ['Debe proporcionar un password.']
        }, status: 401
      end     

      @user = current_user_sender

      if @user


        #password_encryptada = BCrypt::Password.create(resource_params[:password])    
        @user.password =  resource_params[:password]
        @user.status = "A" 
        params_password = {"encrypted_password"=>resource_params[:password],"status"=>'A'}

       # if @user.update_attributes(params_password)
        if @user.save
          ActionCorreo.reset_password(@user).deliver_now
          return render json: {
            success: true,
            data: {
              user: @user,
              message: "Cambio de Clave Realizado con Exito."
            }
          }
        else
          return render json: {
            success: false,
            errors: @user.errors
          }, status: 422
        end

      end

    end

    def resource_params
      params.permit(:email, :password)
    end
  
  end
end