class Api::V1::MessagesController < Api::V1::BaseController
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :set_user_by_token
  before_action :authenticate_user_sender!

  respond_to :json

  def index
    return render json: {
        success: true,
        data: {
          messages: resource_klass.where(sender_id: current_user_sender.id)
          }
      } 
  end

end