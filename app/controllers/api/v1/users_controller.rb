class Api::V1::UsersController < Api::V1::BaseController
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :set_user_by_token
  before_action :authenticate_user_sender!

  respond_to :json

  # info of autenticated user
  def me
    respond_with current_user_sender
  end

  def help_us

    # Enviar Correo
    HelpUs.notification(UserSender.last, "Hola Mundo").deliver_later

    render json: {info: "Gracias"}, status: :accepted
  end

  # POST /api/{plural_resource_name}
  def create
    set_resource(resource_klass.new(resource_params))

    if get_resource.save     
      render json: {id: get_resource.id}, status: :created
    else
      render json: get_resource.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/{plural_resource_name}/1
  def update  
    if get_resource.update(resource_params)
      render json: {
            success: true,
            id: get_resource.id
          }, status: :accepted
    else
      render json: {
            success: false,
            errors: get_resource.errors
          }, status: :unprocessable_entity
    end
  end  


  # GET /api/{plural_resource_name}
  def index
    plural_resource_name = "@#{resource_name.pluralize}"

    #byebug
    if query_params[:lat] && query_params[:lon]
      resources = resource_klass.near([query_params[:lat], query_params[:lon]])
                              .page(page_params[:page])
                              .per(page_params[:page_size])

    else
      resources = []
                  # resource_klass.where(query_params)
                  #             .order(:id => :desc)
                  #             .page(page_params[:page])
                  #             .per(page_params[:page_size])
    end

    instance_variable_set(plural_resource_name, resources)
    respond_with instance_variable_get(plural_resource_name)
  end

  

private

  def user_params
    params.require(:user).permit(
      :name, 
      :facebook_key,
      :photo,
      :provider,
      :push_on,
      :cut_day, 
      :sms_by_cut)
  end

  def query_params
    params.permit(:lat, :lon)
  end

  def resource_klass
    @resource_klass = UserSender
  end

  def parse_image_data(image_data)
    @tempfile = Tempfile.new('item_image')
    @tempfile.binmode
    @tempfile.write Base64.decode64(image_data[:content])
    @tempfile.rewind

    uploaded_file = ActionDispatch::Http::UploadedFile.new(
      tempfile: @tempfile,
      filename: image_data[:filename]
    )

   uploaded_file.content_type = image_data[:content_type]
    uploaded_file
  end

  def clean_tempfile
    if @tempfile
      @tempfile.close
      @tempfile.unlink
    end
  end

end