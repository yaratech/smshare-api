class ActionCorreo < ApplicationMailer

  ADMIN_MAIL = "info@dominio.com"

  def bienvenido_email()
    mail(from: "no-reply@dominio.com", to: ActionCorreo::ADMIN_MAIL, subject: 'Prueba de envio de Correo')
  end

  def user_create(resource) 
    @resource = resource 
    mail(from: "no-reply@dominio.com", to: @resource.email, subject: 'Usuario creado con Exito')
  end

  def request_password(resource, password)
    @resource = resource
    @password = password  
    mail(from: "no-reply@dominio.com", to: @resource.email, subject: "Restablecer la Contraseña")
  end

  def reset_password(resource)
    @resource = resource
    mail(from: "no-reply@dominio.com", to: @resource.email, subject: "Cambio de Contraseña")
  end

  
  def contact(name, email, message)
    @admin_mail = ActionCorreo::ADMIN_MAIL
    @name = name
    @email = email
    @message = message
    mail(from: "no-reply@dominio.com", to: @admin_mail, subject: "Mensaje de Usuario desde Landing")
  end  

  def comment(name, email, message)
    @admin_mail = ActionCorreo::ADMIN_MAIL
    @name = name
    @email = email
    @message = message
    mail(from: "no-reply@dominio.com", to: @admin_mail, subject: "Comentario de Usuario desde Aplicación Móvil")
  end  


  def subscribe(email)
    @admin_mail = ActionCorreo::ADMIN_MAIL
    @email = email
    mail(from: "no-reply@dominio.com", to: @admin_mail, subject: "Solicitud Suscripcion Usuario desde Landing")
  end  


end
