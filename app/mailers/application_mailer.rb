class ApplicationMailer < ActionMailer::Base
  default from: "no_reply@dominio.com"
  layout 'mailer'
end
